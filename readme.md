用户状态：

- 空闲（idle）
- 呼叫中（dialing）
- 通话中（busy）

### 建立连接
通过用户ID在Socket服务器端标记该用户状态：空闲(idle)

### 用户A请求与用户B通话
1. Socket服务器端调整用户A的状态为“呼叫中”；
2. Socket服务器端判断B方状态是否为“空闲”；
3. 如果在线，则通知B来自A的通话请求，并将用户B的状态调整为“呼叫中”；
4. 如果B **同意** 通话，用户A、B的状态调整为“通话中”，由客户端自行建立视频通话连接；
5. 如果B **不同意** 通话，通知用户A结果，并将用户A、B的状态调整为“空闲”；

【TODO】
1. Socket服务器向HTTP服务器发送创建通话请求记录(用户A的ID、用户B的ID，连接是否成功)；

### 发送事件
#### emit('login', user)

登录Socket服务器。将会向所有用户（除了用户自己）广播"userConnect"事件。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 user |  object | 用户信息，必须要有name和id。 | {"id": 30, "name": "Ivan", xxx: yyy, ...}

#### emit('dial', parameters)

请求与某个用户建立通话。会返回发送事件'dialResponse'。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
parameters | object | 相关参数。toUserId为必需字段，表示被拨用户id；type为必需字段，当它为“video”的时候才会以视频通话处理，其他情况下将不会修改用户状态，将请求原样推送给id为toUserId的用户。 | {toUserId: 30, type: "video"}；{toUserId: 30, type: "notification", content: "abcd....."}

#### emit('agreeToAnswer', requestId)

接受通话请求。将会向主拨用户发送"callRequestAccepted"事件。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 requestId |  integer | 通话请求id。 | 1

#### emit('refuseToAnswer', requestId)

拒绝通话请求。将会向主拨用户发送"callRequestRefused"事件。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 requestId |  integer | 通话请求id。 | 1

#### emit('endCall', requestId)
通话双方任何一方可以结束通话。将会向通话双方发送"callEnded"事件。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 requestId |  integer | 通话请求id。 | 1

#### emit('listConnectedUsers')
查询用户列表。将会向主拨用户发送"connectedUsers"事件。

### 监听事件

#### on('userConnect', function(user) { xxx })
有用户上线。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 user |  object | 用户信息。 | {"id": 30, "name": "Ivan", socket: {...}, ...}

#### on('userDisconnect', function(user) { xxx })
有用户下线（断线）。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 user |  object | 用户信息。 | {"id": 30, "name": "Ivan", socket: {...}, ...}

#### on('dialed', function(request){ xxx })

接收到通话请求，经过用户处理需发送'agreeToAnswer'或者'refuseToAnswer'事件，如果超过30秒没有响应，该通话请求将失效。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 request |  object | 通话请求。 | {id: 1, fromUserId: 30, toUserId: 31, parameters: {toUserId: 31, type: "video"}, status: 'waiting'}

#### on('dialResponse', function(response) { xxx })

请求通话之后，服务器经过查询，会返回该用户的在线状态及请求传递结果。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 response |  object | 请求传递结果。 | { "success": true, "status": "idle" } ; { "success": false, "status": "busy" }

#### on('callRequestAccepted', function(request) { xxx })
被拨用户接受了您的邀请。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 request |  object | 通话请求。 | {id: 1, fromUserId: 30, toUserId: 31, parameters:  {toUserId: 31, type: "video"}, status: 'accepted'}

#### on('callRequestRefused', function(request) { xxx })
被拨用户拒绝了您的邀请。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 request |  object | 通话请求。 | {id: 1, fromUserId: 30, toUserId: 31, parameters:  {toUserId: 31, type: "video"}, status: 'refused'}

#### on('answerError', function(response) { xxx })
接受或者拒绝通话邀请时出现错误。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 response |  object | 返回结果及相关请求详情。 | 请求不存在： { "success": false, "request": null } ; 请求已处理完毕： { "success": false, "request": {id: 1, fromUserId: 30, toUserId: 31, parameters:  {toUserId: 31, type: "video"}, status: 'refused'} }

#### on('connectedUsers', function(users) { xxx } )
查询在线用户列表。

 字段 | 类型 | 说明 | 举例
 ---- | ---- | ---- | ----
 users |  object | 用户列表。 | {"30": {id: 30, name: "Ivan", socket: {...}}, "44": {id: 44, name: "Koi", socket: {...}}}