var app = require('http').createServer();
var io = require('socket.io')(app);
// var fs = require('fs');
var _ = require('lodash');

var forever = require('forever-monitor');

var child = new (forever.Monitor)('io.js', {
  max: 3,
  silent: true,
  options: []
});

child.on('exit', function () {
  console.log('io.js has exited after 3 restarts');
});

child.start();

app.listen(2222);

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

var users = {};
var requests = {};

findUserBySocket = function (socket) {
  return _.find(_.values(users), {socket: socket});
}

io.on('connection', function (socket) {

  socket.on('login', function (user) {
    user.socket = socket.id;
    user.status = 'idle';
    users[user.id] = user;
    socket.broadcast.emit("userConnect", user);
  });

  // 通知被拨用户推送请求
  socket.on('dial', function(params) {
    var fromUser = findUserBySocket(socket.id);
    if (params.type == "video") {
      fromUser.status = 'dialing';
    }

    var toUser = users[params.toUserId];
    var ifSuccess = false, status = "disconnected";
    var requestId;
    requestId = new Date().getTime();
    requests[requestId] = {
      id: requestId,
      fromUserId: fromUser.id,
      toUserId: params.toUserId,
      parameters: params,
      status: "waiting"
    };

    if (toUser) {
      status = toUser.status;

      if (toUser.status == 'idle' && params.type == "video") {

        toUser.status = 'dialing';

        ifSuccess = true;
      }
      // 通知被拨用户请求
      io.sockets.connected[toUser.socket].emit('dialed', requests[requestId]);
    }

    // 通知主播用户通话请求传递结果
    io.sockets.connected[fromUser.socket].emit('dialResponse', {
      status: status,
      success: ifSuccess,
      request: requests[requestId]
    });

    if (!ifSuccess) {fromUser.status = 'idle'; requests[requestId] = null};
  });

  // 接受通话请求
  socket.on('agreeToAnswer', function (requestId) {
    if (requests[requestId] && requests[requestId].status == 'waiting') {
      requests[requestId].status = "agreed";
      fromUser = users[requests[requestId].fromUserId];
      toUser = users[requests[requestId].toUserId];
      fromUser.status = "busy";
      toUser.status = "busy";
      io.sockets.connected[fromUser.socket].emit('callRequestAccepted', requests[requestId]);
    } else {
      socket.emit("answerError", {success: false, request: requests[requestId]})
    }
  });

  // 拒绝通话请求
  socket.on('refuseToAnswer', function (requestId) {
    if (requests[requestId] && requests[requestId].status == 'waiting') {
      requests[requestId].status = "refused";
      fromUser = users[requests[requestId].fromUserId];
      toUser = users[requests[requestId].toUserId];
      fromUser.status = "idle";
      toUser.status = "idle";
      io.sockets.connected[fromUser.socket].emit('callRequestRefused', requests[requestId]);
    } else {
      socket.emit("answerError", {success: false, request: requests[requestId]})
    }
  });

  // 拒绝通话请求
  socket.on('endCall', function (requestId) {
    if (requests[requestId] && ['waiting', 'agreed'].indexOf(requests[requestId].status) != -1) {
      requests[requestId].status = "end";
      fromUser = users[requests[requestId].fromUserId];
      toUser = users[requests[requestId].toUserId];
      fromUser.status = "idle";
      toUser.status = "idle";
      io.sockets.connected[fromUser.socket].emit('callEnded', requests[requestId]);
      io.sockets.connected[toUser.socket].emit('callEnded', requests[requestId]);
    } else {
      socket.emit("endCallError", {success: false, request: requests[requestId]})
    }
  });

  // 查询用户列表
  socket.on('listConnectedUsers', function () {
    socket.emit("connectedUsers", users);
  });

  socket.on('disconnect', function () {
    user = findUserBySocket(socket.id);
    if (user) {
      socket.broadcast.emit("userDisconnect", user);
      delete users[user.id];
    }
  });
});
