var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(3000);

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/user1', function (req, res) {
  res.sendFile(__dirname + '/user1.html');
});

app.get('/user2', function (req, res) {
  res.sendFile(__dirname + '/user2.html');
});

io.on('connection', function (socket) {
  clients[socket.id] = socket;

  // then you can later do something like this:

  // var socket = clients[sId];
  // socket.emit('show', {});

  io.emit('this', { will: 'be received by everyone'});

  socket.on('private message', function (from, msg) {
    console.log('I received a private message by ', from, ' saying ', msg);
  });

  socket.on('disconnect', function () {
    io.sockets.emit('user disconnected');
  });
});